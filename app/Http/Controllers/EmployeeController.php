<?php

namespace App\Http\Controllers;
use App\Models\Employee;

use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    public function showDataPekerja() {
        $data = Employee::all();
        return view('data-pekerja', compact('data'));
    }

    public function tambahData() {
        return view('tambah');
    }

    public function simpanData(Request $request) {
        $data = new Employee;
        $data->id_emp = $request->input('id-emp');
        $data->nama = $request->input('nama');
        $data->posisi = $request->input('posisi');
        $data->perusahaan = $request->input('perusahaan');
        $data->save();
        return redirect('/data-pekerja');
    }

    public function editData($slug) {
        $data = Employee::find($slug);
        return view('edit', compact('data'));
    }

    public function simpanEditData(Request $request, $slug) {
        $data = Employee::find($slug);
        $data->id_emp = $request->input('id-emp');
        $data->nama = $request->input('nama');
        $data->posisi = $request->input('posisi');
        $data->perusahaan = $request->input('perusahaan');
        $data->update();
        return redirect('/data-pekerja');
    }

    public function hapusData($slug) {
        $data = Employee::find($slug);
        $data->delete();
        return redirect('data-pekerja');
    }

}
