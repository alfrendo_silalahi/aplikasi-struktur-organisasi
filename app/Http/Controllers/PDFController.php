<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;

class PDFController extends Controller
{
    public function generatePDF() {
        $data = Employee::all();

        $pdf = PDF::loadView('myPDF', compact('data'));

        return $pdf->download('Data.pdf');
    }
}
